from qiskit import *
from qiskit.visualization import *
from qiskit.quantum_info import Statevector
from qiskit_experiments.library import StateTomography # comes from qiskit-experiments package
import numpy as np
import matplotlib.pyplot as plt

plt.style.use('dark_background')

def encode_ascii_latitudinal(letter):
    offset = ord(letter) - 65
    assert offset >= 0
    assert offset <= 25, "letter must be between A-Z"
    return (offset / 25 * np.pi, 0, 0)

def get_spherical_coordinates(statevector):
    # Convert to polar form:
    r0 = np.abs(statevector[0])
    φ0 = np.angle(statevector[0])

    r1 = np.abs(statevector[1])
    φ1 = np.angle(statevector[1])

    # Calculate the coordinates:
    r = np.sqrt(r0 ** 2 + r1 ** 2)
    θ = 2 * np.arccos(r0 / r)
    φ = φ1 - φ0
    return [r, θ, φ]

def naive_state_from_prob(prob):
    return [np.sqrt(1 - prob), np.sqrt(prob)]

def decode_ascii_latitudinal(prob):
    statevec = naive_state_from_prob(prob)
    offset = get_spherical_coordinates(statevec)[1] / np.pi * 25
    return chr(65 + round(offset))

# TODO
# def decode_ascii_spherical(statevec):

def counts_to_states(N, shots, counts):
    probs = [0] * N
    for (measurement, count) in counts.items():
        for (i, q) in enumerate(reversed(measurement)):
            if q == '1': # sum up all occurrences of 1 for qubit i
                probs[i] += count/shots
    return probs

def inverseU(theta, phi, lamb):
    return (-theta, -lamb, -phi)

# parties.
message = "ABC"
key_alice = "HJK"
N = len(message)
assert N == len(key_alice)

print(f"ENCRYPTING MESSAGE {message}...")

qc = QuantumCircuit(N)

for (i, (t, p, l)) in enumerate(map(encode_ascii_latitudinal, message)):
    qc.u(t, p, l, i)

if N != 1:
    for i in range(N):
        # Apply a U3 gate parameterized by the secret
        # key to qubit i
        qc.u(*encode_ascii_latitudinal(key_alice[i]), i)
        # Apply a CNOT from the previous letter's qubit
        # to the next
        qc.cx(i, (i + 1) % N)

qst = StateTomography(qc)
backend = Aer.get_backend('aer_simulator')
data = qst.run(backend, shots=50).block_for_results()

print("Result density matrix from Alice's state tomography:")
rho = data.analysis_results("state").value
# display(plot_state_city(rho))

print("Encryption measurement coherence:", np.trace(rho._data @ rho._data))

# An absolute tolerance of ~0.05 is required for
# consistent results
statevec = rho.to_statevector(atol=0.5)