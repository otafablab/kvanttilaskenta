use csv::Writer;
use nalgebra::allocator::Allocator;
use nalgebra::*;
use serde::Serialize;
use std::f32::consts::*;

fn arbitrary_renormalized(num_qubits: usize, max_phi: f32) -> DVector<Complex<f32>> {
    let amplitudes = DVector::<f32>::new_random(2usize.pow(num_qubits as u32))
        .normalize()
        .cast();

    let mut phases: Matrix<Complex<f32>, _, _, _> =
        DVector::<f32>::new_random(2usize.pow(num_qubits as u32))
            .map(|phi| Complex::new(0.0, phi * max_phi).exp())
            .cast();
    phases[0] = Complex::default(); // erase global phase from first qubit

    amplitudes.component_mul(&phases)
}

const E: f32 = 0.00000001;

fn von_neumann_entropy_eig<D, S>(rho: SquareMatrix<f32, D, S>) -> f32
where
    // T: iel,
    S: Storage<f32, D, D>,
    D: DimSub<U1>,
    DefaultAllocator: Allocator<f32, D, DimDiff<D, U1>>
        + Allocator<f32, DimDiff<D, U1>>
        + Allocator<f32, D, D>
        + Allocator<Complex<f32>, D>
        + Allocator<f32, D>,
{
    let eigvals = rho.symmetric_eigenvalues();
    -eigvals
        .into_iter()
        .map(|&d| d.abs() * ln_0_0(d.abs())) // FIXME abs
        .sum::<f32>()
}

fn ln_0_0(num: f32) -> f32 {
    let log = num.ln();
    if log == -f32::INFINITY {
        0.0
    } else {
        log
    }
}

/// Calculates the entanglement entropy of the last qubit to the rest of the system
fn von_neumann_entropy_schmidt(psi: DVector<Complex<f32>>) -> f32 {
    let (dim, _) = psi.shape();
    let rows = dim / 2;

    let schmidt_matrix_last = psi.reshape_generic(Dynamic::new(rows), Dynamic::new(2));

    let singular_values = schmidt_matrix_last
        .svd_unordered(false, false)
        .singular_values;

    -singular_values
        .into_iter()
        .map(|&d| d.powf(2.0) * ln_0_0(d.powf(2.0)))
        .sum::<f32>()
}

/// Traces out the first qubit from the density matrix.
///
/// https://physics.stackexchange.com/questions/179671/how-to-take-partial-trace
fn partial_trace_one(rho: &DMatrix<f32>) -> DMatrix<f32> {
    let d = rho.shape().0;
    let mut sum = DMatrix::<f32>::zeros(d / 2, d / 2);
    let kron_id = DMatrix::<f32>::identity(d / 2, d / 2);
    for basis in Matrix2::identity().column_iter() {
        let lhs = kron_id.kronecker(&basis.adjoint());
        let rhs = kron_id.kronecker(&basis);
        sum += lhs * rho * rhs;
    }
    sum
}

const MAX_QUBITS: usize = 8;
fn generate_qubit_entropy_data() -> Result<(), Box<dyn std::error::Error>> {
    #[derive(Serialize)]
    struct Sample {
        num_qubits: usize,
        ent_entropy: f32,
    }

    let mut wtr = Writer::from_path("entropies.csv")?;
    let mut data = vec![];
    for num_qubits in 2..=MAX_QUBITS {
        for _ in 0..1000 {
            let psi = arbitrary_renormalized(num_qubits, 2.0 * PI);
            let ent_entropy = von_neumann_entropy_schmidt(psi);

            data.push(Sample {
                num_qubits,
                ent_entropy,
            });
        }
        println!("finished {num_qubits}");
    }

    for sample in data {
        wtr.serialize(sample)?;
    }
    wtr.flush()?;

    Ok(())
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    #[derive(Serialize)]
    struct Sample {
        max_phi: f32,
        ent_entropy: f32,
    }

    const MAX_PHI_SAMPLES: u32 = 2;
    const NUM_QUBITS: usize = 20;

    let mut wtr = Writer::from_path("entropies.csv")?;
    let mut data = vec![];
    for max_phi in (0..MAX_PHI_SAMPLES).map(|s| s as f32 / MAX_PHI_SAMPLES as f32 * 2.0 * PI) {
        for _ in 0..100 {
            let psi = arbitrary_renormalized(NUM_QUBITS, max_phi);
            let ent_entropy = von_neumann_entropy_schmidt(psi);

            data.push(Sample {
                max_phi,
                ent_entropy,
            });
        }
        println!("finished {max_phi}");
    }

    for sample in data {
        wtr.serialize(sample)?;
    }
    wtr.flush()?;

    Ok(())
}
